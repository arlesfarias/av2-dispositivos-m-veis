package com.example.professor.av2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.GpsSatellite;
import android.util.AttributeSet;
import android.view.View;

import java.util.LinkedList;

/**
 * Created by arles on 14/11/15.
 */
public class EsferaCeleste extends View {

    private LinkedList<GpsSatellite> gpsSatellites;
    private Paint paint;

    public EsferaCeleste(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.gpsSatellites = new LinkedList<>();
        this.paint = new Paint();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float Xc, Yc, raioCirculo;
        double azimuth, x, y;

        //pega centro da view
        Xc = this.getWidth() / 2;
        Yc = this.getHeight() / 2;

        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(10);
        paint.setTextSize(40);

        //desenha círculo na esfera celeste
        paint.setColor(Color.LTGRAY);
        raioCirculo = (getWidth() - 10) / 2;
        canvas.drawCircle(Xc, Yc, raioCirculo, paint);

        //desenha origem
        paint.setColor(Color.BLACK);
        canvas.drawPoint(Xc, Yc, paint);

        paint.setStyle(Paint.Style.FILL);

        //desenha o prn de cada satélide no seu respectivo local do espaço
        if (!gpsSatellites.isEmpty()) {

            for (GpsSatellite satellite : gpsSatellites) {

                azimuth = Math.toRadians(satellite.getAzimuth());
                raioCirculo = (getWidth() - 10) / 2;
                raioCirculo = (float) (raioCirculo * Math.cos(Math.toRadians(satellite.getElevation())));

                x = Math.cos(azimuth) * (raioCirculo - 50) + Xc;
                y = this.getWidth() - (Math.sin(azimuth) * (raioCirculo - 50) + Yc);

                canvas.drawText(String.valueOf(satellite.getPrn()), (float) x, (float) y, paint);

            }

        }

    }

    public void atualizarValoes(LinkedList<GpsSatellite> gpsSatellites) {

        this.gpsSatellites = gpsSatellites;
        invalidate();

    }
}
