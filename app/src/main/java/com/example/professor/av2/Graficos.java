package com.example.professor.av2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.GpsSatellite;
import android.util.AttributeSet;
import android.view.View;

import java.util.LinkedList;

/**
 * Created by arles on 13/11/15.
 */
public class Graficos extends View {

    private Paint paint;
    private Rect barra;
    private LinkedList<GpsSatellite> gpsSatellites;

    public Graficos(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.gpsSatellites = new LinkedList<>();
        this.paint = new Paint();
        this.barra = new Rect();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //se não testar essa condição pode ocorrer divisão por 0
        if (!gpsSatellites.isEmpty()) {

            int larguraBarra, alturaBarra, espacamentoBarras, base, baseBarra, left = 0, right, quantidade;

            quantidade = this.gpsSatellites.size();
            espacamentoBarras = 10;
            larguraBarra = (this.getWidth() - quantidade * 10) / quantidade;
            base = this.getHeight();
            baseBarra = base - 50;

            paint.setStyle(Paint.Style.FILL);
            paint.setTextSize(40);


            for (GpsSatellite satellite : gpsSatellites) {

                //desenha barra, caso o satélite estiver sendo usado in fix ele será pintado de preto
                alturaBarra = this.getHeight() - Math.round(satellite.getSnr()) * 10;
                right = left + larguraBarra;
                paint.setColor(Color.GRAY);
                barra.set(left, alturaBarra, right, baseBarra);

                if (satellite.usedInFix()) paint.setColor(Color.BLACK);

                canvas.drawRect(barra, paint);

                //desenha texto com SNR
                paint.setColor(Color.BLUE);
                alturaBarra -= 20;

                if (satellite.getSnr() > 0)
                    canvas.drawText(String.valueOf((int) satellite.getSnr()), left, alturaBarra, paint);

                //desenha texto com PRN
                canvas.drawText(String.valueOf(satellite.getPrn()), left, base, paint);

                //prepara left para a próxima iteração
                left += larguraBarra + espacamentoBarras;

            }
        }

    }

    public void atualizarValoes(LinkedList<GpsSatellite> gpsSatellites) {

        this.gpsSatellites = gpsSatellites;
        invalidate();

    }


}

