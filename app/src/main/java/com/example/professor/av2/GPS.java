package com.example.professor.av2;

import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.LinkedList;

public class GPS extends AppCompatActivity implements LocationListener, GpsStatus.Listener {

    private LocationManager locationManager;
    private LocationProvider locationProvider;
    private TextView tvLongitude;
    private TextView tvLatitude;
    private TextView tvAltitude;
    private TextView tvSatellitesVisiveis;
    private TextView tvSatellitesEmUso;
    private LinkedList<GpsSatellite> satellitesVisiveis = new LinkedList<>();
    private LinkedList<GpsSatellite> satellitesEmUso = new LinkedList<>();
    private Graficos graficos;
    private EsferaCeleste esferaCeleste;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);
        tvLongitude = (TextView) findViewById(R.id.tv_longitude);
        tvLatitude = (TextView) findViewById(R.id.tv_latitude);
        tvAltitude = (TextView) findViewById(R.id.tv_altitude);
        tvSatellitesVisiveis = (TextView) findViewById(R.id.tv_satellites_visiveis);
        tvSatellitesEmUso = (TextView) findViewById(R.id.tv_satellites_em_uso);
        graficos = (Graficos) findViewById(R.id.graficos);
        esferaCeleste = (EsferaCeleste) findViewById(R.id.esfera_celeste);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (locationManager.isProviderEnabled(locationProvider.getName())) {
            try {
                locationManager.requestLocationUpdates(locationProvider.getName(), 3000, 1, this);
                locationManager.addGpsStatusListener(this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (locationManager.isProviderEnabled(locationProvider.getName())) {
            try {
                locationManager.removeUpdates(this);
                locationManager.removeGpsStatusListener(this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_g, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGpsStatusChanged(int event) {

        satellitesVisiveis.clear();
        satellitesEmUso.clear();

        GpsStatus gpsStatus = locationManager.getGpsStatus(null);
        Iterable<GpsSatellite> gpsSatellites = gpsStatus.getSatellites();

        for (GpsSatellite satellite : gpsSatellites) {
            satellitesVisiveis.add(satellite);
            if (satellite.usedInFix()) satellitesEmUso.add(satellite);
        }

        tvSatellitesVisiveis.setText("Satélites em visíveis: " + satellitesVisiveis.size());
        tvSatellitesEmUso.setText("Satélites em uso: " + satellitesEmUso.size());
        graficos.atualizarValoes(satellitesVisiveis);
        esferaCeleste.atualizarValoes(satellitesVisiveis);

    }

    @Override
    public void onLocationChanged(Location location) {
        tvLongitude.setText(getResources().getString(R.string.longitude) + " " + location.getLongitude());
        tvLatitude.setText(getResources().getString(R.string.latitude) + " " + location.getLatitude());
        tvAltitude.setText(getResources().getString(R.string.altitude) + " " + location.getAltitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
