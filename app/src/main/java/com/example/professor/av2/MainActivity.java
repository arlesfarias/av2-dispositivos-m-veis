package com.example.professor.av2;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnGPS = (Button) findViewById(R.id.btnGps);
        Button btnNavegacao = (Button) findViewById(R.id.btnNavegacao);
        Button btnCreditos = (Button) findViewById(R.id.btnCreditos);
        Button btnSair = (Button) findViewById(R.id.btnSair);
        btnGPS.setOnClickListener(this);
        btnNavegacao.setOnClickListener(this);
        btnCreditos.setOnClickListener(this);
        btnSair.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnCreditos:
                Intent cred = new Intent(this, Creditos.class);
                startActivity(cred);
                break;
            case R.id.btnNavegacao:
                if (haveConnection()) {
                    Intent nav = new Intent(this, Navegacao.class);
                    startActivity(nav);
                } else {
                    Toast.makeText(this, "Sem conexão!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnGps:
                Intent gps = new Intent(this,GPS.class);
                startActivity(gps);
                break;
            case R.id.btnSair:
                finish();
                break;
        }
    }

    private boolean haveConnection() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
