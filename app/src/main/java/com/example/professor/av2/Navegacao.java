package com.example.professor.av2;

import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

public class Navegacao extends AppCompatActivity implements LocationListener, GpsStatus.Listener {

    private MapFragment mapFragment;
    private GoogleMap map;
    private TextView tvNavLocal, tvNavSat;
    private LocationManager locationManager;
    private LocationProvider locationProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegacao);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        map = mapFragment.getMap();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationProvider = locationManager.getProvider(LocationManager.GPS_PROVIDER);

        tvNavLocal = (TextView) findViewById(R.id.tv_nav_info_local);
        tvNavSat = (TextView) findViewById(R.id.tv_nav_info_sat);

        tvNavLocal.setText("Obtendo localização...");
        tvNavSat.setText("Obtendo informações sobre os satélites...");


        map.setMyLocationEnabled(true);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (locationManager.isProviderEnabled(locationProvider.getName())) {
            try {
                locationManager.requestLocationUpdates(locationProvider.getName(), 3000, 1, this);
                locationManager.addGpsStatusListener(this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (locationManager.isProviderEnabled(locationProvider.getName())) {
            try {
                locationManager.removeUpdates(this);
                locationManager.removeGpsStatusListener(this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_navegacao, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGpsStatusChanged(int event) {

        GpsStatus gpsStatus = locationManager.getGpsStatus(null);
        Iterable<GpsSatellite> satellites = gpsStatus.getSatellites();
        int satVisiveis = 0, satUtilizados = 0;
        StringBuilder prn = new StringBuilder();

        for (GpsSatellite satellite : satellites) {
            satVisiveis++;
            if (satellite.usedInFix()) satUtilizados++;
            prn.append(satellite.getPrn());
            prn.append(" ");
        }

        tvNavSat.setText("Satélites visíveis: " + satVisiveis + "\n");
        tvNavSat.append("Satélites utilizados: " + satUtilizados + "\n");
        tvNavSat.append("Lista de PRNs: " + prn.toString());


    }

    @Override
    public void onLocationChanged(Location location) {

        double latitude, longitude;

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        tvNavLocal.setText("Latitude: " + latitude + "\nLongitude: " + longitude);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
